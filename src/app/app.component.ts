import {ChangeDetectorRef, Component, NgZone} from '@angular/core';
import 'rxjs/add/operator/map';
import ItemRead = Office.ItemRead;
import AsyncResult = Office.AsyncResult;
import CoercionType = Office.CoercionType;
import {DomSanitizer} from "@angular/platform-browser";
import {BasePropertySet, FileAttachment, ItemSchema, PropertySet} from "ews-js-api-browser";
import * as OfficeHelpers from "@microsoft/office-js-helpers";
import IToken = OfficeHelpers.IToken;
import {HttpClient, HttpHeaders} from "@angular/common/http";


declare const EwsJS: any;


export interface ServiceRequest {
  attachmentToken: string,
  ewsUrl: string
  attachments: any[]
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  // AppComponent properties
  exchangeUrl: string;
  attachments: any;
  symbols: string[] = [];
  error: string = null;
  waiting = false;
  mail: ItemRead;
  body: any;
  private serviceRequest: ServiceRequest;
  protected selectedAttachment: any;
  private exch: any;
  protected token: IToken;
  protected contentToPreview: string;
  // zone: NgZone = new NgZone({});

  protected countActive = document.getElementsByClassName('active').length;
  // AppComponent constructor
  constructor(private cdRef: ChangeDetectorRef,
              protected domSanitizer: DomSanitizer,
              private http: HttpClient) {

    this.serviceRequest = {
      attachmentToken: '',
      ewsUrl: '',
      attachments: []
    };

    this.mail = Office.context.mailbox.item as ItemRead;
    this.serviceRequest.ewsUrl = Office.context.mailbox.ewsUrl;
    this.attachments = (Office.context.mailbox.item as ItemRead).attachments;
    this.serviceRequest.attachments = (Office.context.mailbox.item as ItemRead).attachments;
    // (Office.context.mailbox.item as ItemRead).body.getAsync(CoercionType.Html, (e) => this.body = e.value);
    const authenticator = new OfficeHelpers.Authenticator();
    authenticator.endpoints.registerMicrosoftAuth('933bd462-8cb1-406f-b870-895a8e7871e7',
      {scope: 'Mail.Read'});
    this.token = authenticator.tokens.get(OfficeHelpers.DefaultEndpoints.Microsoft);
    const __this = this;

    authenticator
      .authenticate(OfficeHelpers.DefaultEndpoints.Microsoft, false)
      .then(function (token) {
        __this.token = token;
        __this.getGivenAttachmentContent(__this.mail.itemId);

      })
      .catch(OfficeHelpers.Utilities.log);


  }

  protected selectAttachment(attachment: any) {
    this.selectedAttachment = attachment;
    this.contentToPreview = atob(attachment.contentBytes);
    this.cdRef.detectChanges();
    const attachmentNode = document.getElementById(attachment.id);
    if (attachmentNode.classList.contains('active')) {
      attachmentNode.classList.remove('active');
    } else {
      attachmentNode.classList.add('active');
    }
    this.countActive = document.getElementsByClassName('active').length;
    // const blob = new Blob([attachment.contentBytes], {type: attachment.contentType});
    // const url = window.URL.createObjectURL(blob);
    // window.open(url);

  }

  private getGivenAttachmentContent(mailId: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json',
        'Authorization': `Bearer ${this.token.access_token}`
      })
    };
    this.http.get(`https://graph.microsoft.com/v1.0/me/messages('${mailId}')/attachments`, httpOptions)
      .subscribe((e: any) => {
        this.attachments = e.value;
        // console.log(this.attachments);
        this.cdRef.detectChanges();
      });


  }

}
