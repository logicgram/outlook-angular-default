///<reference path="../node_modules/@types/node/index.d.ts"/>

import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import * as OfficeHelpers from '@microsoft/office-js-helpers';
import {AppModule} from './app/app.module';
import {environment} from './environments/environment';

if (environment.production) {
  enableProdMode();
}

Office.initialize = function () {
  EwsJS.ConfigureForOutlook();
  if (OfficeHelpers.Authenticator.isAuthDialog()) return;
  platformBrowserDynamic().bootstrapModule(AppModule)
    .catch(err => console.log(err));
};
